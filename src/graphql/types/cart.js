module.exports.typeDef = `
  type CartProduct {
    name: String,
    price: Float,
    quantity: Int,
    total: Float
  }
  type Cart {
    id: Int,
    products: [CartProduct],
    total: Float
  }
  type Query {
    cart(id: Int!): Cart
  }
  type Mutation {
    addProduct(cartId: Int, productName: String!): Cart!
    removeProduct(cartId: Int, productName: String!): Cart!
  }
`;

const { makeExecutableSchema } = require('graphql-tools');

const { typeDef: Product } = require('./product');
const { typeDef: Cart } = require('./cart');

const productSchema = makeExecutableSchema({
  typeDefs: Product
});
const cartSchema = makeExecutableSchema({
  typeDefs: Cart
});

module.exports.schemas = [
  productSchema,
  cartSchema
];

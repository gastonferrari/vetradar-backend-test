# vetradar-backend-test

Solution for VetRadar Backend Test v1.1

## Instructions
To run the app

```
npm i && npm start
```

To run the tests

```
npm test
```

Once running, graphiQL can be accessed at
```http://localhost:3000/graphql```

To drop the database just delete all .db files in the root of the project

## Query examples

```
{
  cart(id:1){
    id,
    products {
      name,
      quantity,
      price,
      total
    },
    total
  }
  products {
    name,
    price
  }
}
```

## Design decisions and problems

1. The election of NeDB as a database was based on set up convenience for the reviewer. At the same time it uses a subset of MongoDB operations, so could be migrated easily. It also makes a bit awkward to create the models, so that part would have to be refactored.

2. Due to lack of time and to make it easier for the reviewer to test the API, there's no user authentication or session. Ideally the cart would be linked to the user, but in this case there's a cart ID required to add or remove products. If no ID is passed when adding a product to the cart, a new cart gets created. The cart IDs are created incrementally, so it's be possible to create a product on a cart that hasn't been created yet. All these issues would be resolved by having a unique cart linked to each user.

3. The API suffers from the N+1 problem, which could be solved by adding the DataLoader library to cache the duplicated DB fetch operations.

4. The tests are very basic and only cover the schema definitions. I'd have needed more time to look into best practices to test the resolvers.

5. The used approach to return the cart after an update is not ideal, again due to lack of time. It would have been better to find a way of reusing the query resolvers.
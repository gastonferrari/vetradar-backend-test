const { ProductModel } = require('./product_model');
const { CartModel } = require('./cart_model');

const models = {};
module.exports.models = models;
module.exports.init = db => {
  models.productModel = new ProductModel(db);
  models.cartModel = new CartModel(db);
};

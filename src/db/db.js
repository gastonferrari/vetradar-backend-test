const Datastore = require('nedb');
const nedbPromise = require('nedb-promise');
const Promise = require('bluebird');

const products = [
  {
    name: 'Sledgehammer',
    price: 125.76
  },
  {
    name: 'Axe',
    price: 190.51
  },
  {
    name: 'Bandsaw',
    price: 562.14
  },
  {
    name: 'Chisel',
    price: 13.9
  },
  {
    name: 'Hacksaw',
    price: 19.45
  }
];

const addProducts = async collection => {
  let names = products.map(p => p.name);
  let dbProducts = await collection.find({ name: { $in: names } });
  let productIndex = {};
  dbProducts.forEach(prod => {
    productIndex[prod.name] = prod;
  });
  await Promise.map(products, prod => {
    if (!productIndex[prod.name]) {
      return collection.insert(prod);
    }
  });
};

module.exports.init = async function () {
  let ds = await Promise.all([
    new Datastore({
      filename: 'products.db',
      autoload: true
    }),
    new Datastore({
      filename: 'cart_products.db',
      autoload: true
    })
  ]);
  const [productsDS, cartsDS] = ds;
  let collections = {};
  collections.products = nedbPromise.fromInstance(productsDS);
  collections.products.ensureIndex({ fieldName: 'name' });
  collections.cartProducts = nedbPromise.fromInstance(cartsDS);
  collections.cartProducts.ensureIndex({ fieldName: 'id' });
  await addProducts(collections.products);
  return collections;
};

class CartModel {
  constructor (collections) {
    this.cartProducts = collections.cartProducts;
  }
  async getCart (id) {
    let cartProducts = await this.cartProducts.find({ cart_id: id });
    return cartProducts;
  }
  async getNewCartId () {
    let carts = await this.cartProducts.update(
      { _id: '__cartid__' },
      { $inc: { seq: 1 } },
      { upsert: true, returnUpdatedDocs: true }
    );
    return carts[1].seq;
  }
  async addProduct (cartId, productId) {
    await this.cartProducts.update(
      { cart_id: cartId, product_id: productId },
      { $inc: { quantity: 1 } },
      { upsert: true }
    );
  }
  async removeProduct (cartId, productId) {
    await this.cartProducts.remove({ cart_id: cartId, product_id: productId });
  }
}

module.exports.CartModel = CartModel;

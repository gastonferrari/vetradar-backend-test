const express = require('express');
const expressGraphQL = require('express-graphql');
const bodyParser = require('body-parser');

const { schema } = require('./src/graphql/graphql');
const db = require('./src/db/db');
const models = require('./src/models/');
db.init()
  .then(collections => models.init(collections))
  .catch(error => {
    console.log('Error connecting to the DB', error);
    process.exit(1);
  });

const app = express();
const port = process.env.PORT || '3000';

app.use(
  '/graphql',
  bodyParser.json(),
  expressGraphQL({
    schema,
    graphiql: true
  })
);

app.listen(port, () => console.log(`Server running on port ${port}`));

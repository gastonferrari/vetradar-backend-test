const { resolvers: Product } = require('./product.js');
const { resolvers: Cart } = require('./cart.js');

module.exports.resolvers = [
  Product,
  Cart
];

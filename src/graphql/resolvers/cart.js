const { models } = require('../../models/');

const getCart = async id => {
  let cart = { id: id, total: 0, products: [] };
  let products = await getCartProducts(id);
  cart.products = products.map(prod => {
    let p = { ...prod };
    cart.total += p.total;
    p.total = p.total.toFixed(2);
    return p;
  });
  cart.total = cart.total.toFixed(2);
  return cart;
};

const getCartProducts = async id => {
  let cartProds = await models.cartModel.getCart(id);
  let prodIds = cartProds.map(p => p.product_id);
  let prods = await models.productModel.getProducts(prodIds);
  let prodIndex = {};
  prods.forEach(p => (prodIndex[p._id] = p));
  let products = [];
  cartProds.forEach(p => {
    let product = prodIndex[p.product_id];
    if (product) {
      let cartProduct = {};
      cartProduct.quantity = p.quantity;
      cartProduct.name = product.name;
      cartProduct.price = product.price;
      cartProduct.total = (cartProduct.price * cartProduct.quantity);
      products.push(cartProduct);
    }
  });
  return products;
};

module.exports.resolvers = {
  Query: {
    cart: async (root, { id }) => {
      return { id: id };
    }
  },
  Cart: {
    products: async (root, { id }) => {
      let products = await getCartProducts(root.id);
      return products.map(prod => {
        let p = { ...prod };
        p.total = p.total.toFixed(2);
        return p;
      });
    },
    total: async (root) => {
      let prods = await getCartProducts(root.id);
      let total = prods.reduce((accum, prod) => accum + prod.total, 0).toFixed(2);
      return total;
    }
  },
  Mutation: {
    addProduct: async (root, { cartId, productName }) => {
      if (!cartId) {
        cartId = await models.cartModel.getNewCartId();
      }
      let product = await models.productModel.getProductByName(productName);
      await models.cartModel.addProduct(cartId, product._id);
      let cart = await getCart(cartId);
      return cart;
    },
    removeProduct: async (root, { cartId, productName }) => {
      let product = await models.productModel.getProductByName(productName);
      await models.cartModel.removeProduct(cartId, product._id);
      let cart = await getCart(cartId);
      return cart;
    }
  }
};

const { mergeSchemas } = require('graphql-tools');

const { resolvers } = require('./resolvers/resolvers');
const { schemas } = require('./types/schemas');

module.exports.schema = mergeSchemas({
  schemas,
  resolvers
});
